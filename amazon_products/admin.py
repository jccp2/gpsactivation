from django.contrib import admin

# Register your models here.
from amazon_products.models import Product


def syncWithAmazon(modeladmin, request, queryset):
    Product.syncWithAmazon(queryset)
    syncWithAmazon.short_description = "Sync Product With Amazon"


class ProductAdmin(admin.ModelAdmin):
    list_display = 'asin', 'title', 'last_updated'
    actions = [syncWithAmazon]


admin.site.register(Product, ProductAdmin)