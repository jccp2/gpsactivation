from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from django.db import models
import amazonproduct
from siteadmin.models import SiteAdmin

config = {
    'access_key': 'AKIAJI3NTILLGE6HPNPQ',
    'secret_key': 'XXWLc1PEP8FOlZWbx1HobvA9m0uqkL7OHTMG/TSa',
    'associate_tag': 'kingtrac-20',
    'locale': 'us'
}
api = amazonproduct.API(cfg=config)

# Create your models here.

class Product(models.Model):
    site = models.ForeignKey(SiteAdmin, related_name='Product')
    asin = models.CharField(_('Product ASIN'), max_length=20)
    url = models.URLField(_('Product URL'), blank=True, null=True)
    title = models.CharField(_('Product Title'), max_length=225, blank=True, null=True)
    image_url = models.URLField(_('Product Image URL'), blank=True, null=True)
    price = models.CharField(_('Product Price'), max_length=10, blank=True, null=True)
    last_updated = models.DateTimeField(default=timezone.now())

    def __unicode__(self):
        return self.asin + ' | ' + self.title

    @staticmethod
    def syncWithAmazon(products, AWSError=None):

        for product in products:
            try:
                result = api.item_lookup(product.asin, ResponseGroup="Medium")
                for item in result.Items.Item:
                    product.title = item.ItemAttributes.Title
                    product.url = item.DetailPageURL
                    product.image_url = item.MediumImage.URL
                    product.price = item.OfferSummary.LowestNewPrice.FormattedPrice
                    product.last_updated = timezone.now()

                    product.save()

            except AWSError, e:
                print 'Amazon complained about your request!'
                print e.code
                print e.msg





