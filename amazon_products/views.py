from django.conf.global_settings import DEFAULT_CONTENT_TYPE
from django.core.serializers import serialize
from django.http import HttpResponse, HttpResponseNotFound
from django.shortcuts import render, render_to_response

from amazon_products.models import Product
from django.core.exceptions import ObjectDoesNotExist


def index(request):
    if request.GET:
        p = Product.objects.filter(site__domain=request.GET.get('domain'))
        if p:
            return HttpResponse(content=serialize('json', p), mimetype=DEFAULT_CONTENT_TYPE)
        else:
            return HttpResponseNotFound(content="NOT FOUND", mimetype=DEFAULT_CONTENT_TYPE)
    else:
        return HttpResponseNotFound(content="NOT WHAT YOU ARE LOOKING FOR", mimetype=DEFAULT_CONTENT_TYPE)
