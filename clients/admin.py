from django.contrib import admin
from clients.models import Client
from activations.models import Activation


class ActivationsInline(admin.TabularInline):
    model = Activation
    extra = 0

class ClientAdmin(admin.ModelAdmin):
    inlines = (ActivationsInline,)


admin.site.register(Client,ClientAdmin)