from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _
from django.db import models


# Create your models here.
class Client(User):
    phone = models.CharField(_('Phone'), max_length=15)
    address = models.CharField(_('address'), max_length=64)
    address_2 = models.CharField(_('Address Cont.'), max_length=64)
    city = models.CharField(_('City'), max_length=64)
    state = models.CharField(_('State'), max_length=2)
    country = models.CharField(_('Country'), max_length=20, default="US")
    zip_code = models.PositiveIntegerField(_('Zip Code'))
    date_created = models.DateTimeField('Date Published')

    def __unicode__(self):  # Python 3: def __str__(self):
        return self.user.first_name + ' ' + self.user.last_name
