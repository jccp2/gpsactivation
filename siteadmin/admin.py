from django.contrib import admin

# Register your models here.
from activations.models import Plan
from siteadmin.models import SiteAdmin

class PlanInline(admin.TabularInline):
    model = Plan
    extra = 1

class SiteAdminAdmin(admin.ModelAdmin):
    inlines = (PlanInline,)

admin.site.register(SiteAdmin,SiteAdminAdmin)