from django.contrib.sites.models import Site
from django.db import models

# Create your models here.

class SiteAdmin(Site):
    admin_email = models.CharField(max_length=225, verbose_name="Admin Email");
    activation_email = models.CharField(max_length=225, verbose_name="GPS Activation Email");
    activation_fee = models.FloatField(max_length=10, verbose_name="Activation Fee")
