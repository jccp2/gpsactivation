# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'SiteAdmin'
        db.create_table(u'siteadmin_siteadmin', (
            (u'site_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['sites.Site'], unique=True, primary_key=True)),
            ('admin_email', self.gf('django.db.models.fields.CharField')(max_length=225)),
            ('activation_email', self.gf('django.db.models.fields.CharField')(max_length=225)),
            ('activation_fee', self.gf('django.db.models.fields.FloatField')(max_length=10)),
        ))
        db.send_create_signal(u'siteadmin', ['SiteAdmin'])


    def backwards(self, orm):
        # Deleting model 'SiteAdmin'
        db.delete_table(u'siteadmin_siteadmin')


    models = {
        u'siteadmin.siteadmin': {
            'Meta': {'ordering': "('domain',)", 'object_name': 'SiteAdmin', '_ormbases': [u'sites.Site']},
            'activation_email': ('django.db.models.fields.CharField', [], {'max_length': '225'}),
            'activation_fee': ('django.db.models.fields.FloatField', [], {'max_length': '10'}),
            'admin_email': ('django.db.models.fields.CharField', [], {'max_length': '225'}),
            u'site_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['sites.Site']", 'unique': 'True', 'primary_key': 'True'})
        },
        u'sites.site': {
            'Meta': {'ordering': "('domain',)", 'object_name': 'Site', 'db_table': "'django_site'"},
            'domain': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        }
    }

    complete_apps = ['siteadmin']