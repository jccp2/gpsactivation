from django.contrib import admin
from activations.models import Activation, Plan

# Register your models here.

class PlanAdmin(admin.ModelAdmin):
    list_display = 'name','type'

admin.site.register(Activation)
admin.site.register(Plan, PlanAdmin)
