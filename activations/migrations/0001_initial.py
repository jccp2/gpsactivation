# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Plan'
        db.create_table(u'activations_plan', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=225)),
            ('amount', self.gf('django.db.models.fields.FloatField')()),
            ('length', self.gf('django.db.models.fields.IntegerField')()),
            ('type', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('site', self.gf('django.db.models.fields.related.ForeignKey')(related_name='Plan', to=orm['siteadmin.SiteAdmin'])),
        ))
        db.send_create_signal(u'activations', ['Plan'])

        # Adding model 'Activation'
        db.create_table(u'activations_activation', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('client', self.gf('django.db.models.fields.related.ForeignKey')(related_name='activations', to=orm['clients.Client'])),
            ('plan', self.gf('django.db.models.fields.related.ForeignKey')(related_name='activations', to=orm['activations.Plan'])),
            ('imei', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('unit', self.gf('django.db.models.fields.CharField')(max_length=25)),
            ('start_date', self.gf('django.db.models.fields.DateTimeField')()),
            ('subscription_id', self.gf('django.db.models.fields.IntegerField')(max_length=225, null=True, blank=True)),
            ('subscription_status', self.gf('django.db.models.fields.CharField')(max_length=225, blank=True)),
            ('amount', self.gf('django.db.models.fields.FloatField')()),
            ('activation_confirmation_number', self.gf('django.db.models.fields.IntegerField')(max_length=225, null=True, blank=True)),
            ('reoccurring_payment_confirmation_number', self.gf('django.db.models.fields.IntegerField')(max_length=225, null=True, blank=True)),
        ))
        db.send_create_signal(u'activations', ['Activation'])


    def backwards(self, orm):
        # Deleting model 'Plan'
        db.delete_table(u'activations_plan')

        # Deleting model 'Activation'
        db.delete_table(u'activations_activation')


    models = {
        u'activations.activation': {
            'Meta': {'object_name': 'Activation'},
            'activation_confirmation_number': ('django.db.models.fields.IntegerField', [], {'max_length': '225', 'null': 'True', 'blank': 'True'}),
            'amount': ('django.db.models.fields.FloatField', [], {}),
            'client': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'activations'", 'to': u"orm['clients.Client']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'imei': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'plan': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'activations'", 'to': u"orm['activations.Plan']"}),
            'reoccurring_payment_confirmation_number': ('django.db.models.fields.IntegerField', [], {'max_length': '225', 'null': 'True', 'blank': 'True'}),
            'start_date': ('django.db.models.fields.DateTimeField', [], {}),
            'subscription_id': ('django.db.models.fields.IntegerField', [], {'max_length': '225', 'null': 'True', 'blank': 'True'}),
            'subscription_status': ('django.db.models.fields.CharField', [], {'max_length': '225', 'blank': 'True'}),
            'unit': ('django.db.models.fields.CharField', [], {'max_length': '25'})
        },
        u'activations.plan': {
            'Meta': {'object_name': 'Plan'},
            'amount': ('django.db.models.fields.FloatField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'length': ('django.db.models.fields.IntegerField', [], {}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '225'}),
            'site': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'Plan'", 'to': u"orm['siteadmin.SiteAdmin']"}),
            'type': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        },
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'clients.client': {
            'Meta': {'object_name': 'Client', '_ormbases': [u'auth.User']},
            'address': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'address_2': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'city': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'country': ('django.db.models.fields.CharField', [], {'default': "'US'", 'max_length': '20'}),
            'date_created': ('django.db.models.fields.DateTimeField', [], {}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '15'}),
            'state': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            u'user_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['auth.User']", 'unique': 'True', 'primary_key': 'True'}),
            'zip_code': ('django.db.models.fields.PositiveIntegerField', [], {})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'siteadmin.siteadmin': {
            'Meta': {'ordering': "('domain',)", 'object_name': 'SiteAdmin', '_ormbases': [u'sites.Site']},
            'activation_email': ('django.db.models.fields.CharField', [], {'max_length': '225'}),
            'activation_fee': ('django.db.models.fields.FloatField', [], {'max_length': '10'}),
            'admin_email': ('django.db.models.fields.CharField', [], {'max_length': '225'}),
            u'site_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['sites.Site']", 'unique': 'True', 'primary_key': 'True'})
        },
        u'sites.site': {
            'Meta': {'ordering': "('domain',)", 'object_name': 'Site', 'db_table': "'django_site'"},
            'domain': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        }
    }

    complete_apps = ['activations']