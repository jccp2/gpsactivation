from django.contrib.sites.models import Site
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, render_to_response
from activations.forms import ActivationForm, CreditCardInfoForm
from django.core.context_processors import csrf
from django.forms.formsets import formset_factory
# Create your views here.
from activations.models import Plan
from siteadmin.models import SiteAdmin
from clients.forms import ClientForm

# def index(request):
#     """
#     ASK JACOB WHY I CANT FILTER TWICE
#     current_site = SiteAdmin.objects.get(domain=Site.Objects.get_current.domain)
#     us_options = Plan.objects.filter(type=0).filter(site__domain=current_site.domain)
#     """
#
#     us_options = Plan.objects.filter(type=0)
#     international_options = Plan.objects.filter(type=1)
#     context = {
#         'us_options': us_options,
#         'international_options': international_options
#     }
#
#     return render(request, 'activation/index.html', context)


def index(request):
    formset = formset_factory(form=ActivationForm, extra=2)
    args = {}
    args.update(csrf(request))
    if request.POST:
        # activation_formset = formset(request.POST, prefix='activations')
        client_form = ClientForm(data=request.POST, prefix='clientForm')
        # cc_form = CreditCardInfoForm(prefix='creditCardInfoForm')

        if client_form.is_valid():
            return HttpResponse(content=request.POST.values());
        else:
            return HttpResponse(content=client_form.error)
    # if activation_formset.is_valid():
    # form.save()
    # return HttpResponseRedirect('/activations/success')
    else:


        args['ActivationFormSet'] = formset(prefix='activations')
        args['ClientForm'] = ClientForm(prefix='clientForm')
        args['CreditCardForm'] = CreditCardInfoForm(prefix='creditCardInfoForm')

        return render_to_response('activation/index.html', args)
