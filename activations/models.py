from django.utils.translation import ugettext_lazy as _
from django.db import models


# Create your models here.
from GPSActivation.utils import Enumeration
from siteadmin.models import SiteAdmin


class Plan(models.Model):
    TYPES = Enumeration(
        (0, 'US_ONLY', 'US Only'),
        (1, 'INTERNATIONAL', 'International'),
    )
    name = models.CharField(max_length=225, unique=True)
    amount = models.FloatField()
    length = models.IntegerField()
    type = models.IntegerField(_('Plan Type'), choices=TYPES, default=0)
    site = models.ForeignKey(SiteAdmin, related_name='Plan')

    def __unicode__(self):
        return self.name


class Activation(models.Model):
    client = models.ForeignKey('clients.Client', related_name='activations')
    plan = models.ForeignKey(Plan, related_name='activations')
    imei = models.CharField(max_length=100)
    unit = models.CharField(max_length=25)
    start_date = models.DateTimeField()
    subscription_id = models.IntegerField(max_length=225, blank=True, null=True)
    subscription_status = models.CharField(max_length=225, blank=True)
    amount = models.FloatField()
    activation_confirmation_number = models.IntegerField(max_length=225, blank=True, null=True)
    reoccurring_payment_confirmation_number = models.IntegerField(max_length=225, blank=True, null=True)


    def __unicode__(self):  # Python 3: def __str__(self):
        return self.client.user.email + ' ' + self.imei

