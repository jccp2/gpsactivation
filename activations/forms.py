from django import forms
from GPSActivation.fields import GroupedModelChoiceField
from activations.models import Activation, Plan


class ActivationForm(forms.Form):
    imei = forms.CharField(required=True, label="Device IMEI")
    plan = GroupedModelChoiceField(queryset=Plan.objects.get_query_set(), group_by_field='type',
                                   group_label=lambda group: Plan.TYPES[group][1])


class CreditCardInfoForm(forms.Form):
    card_number = forms.IntegerField(label='Credit Card Number')
    card_exp_date = forms.CharField(label='Exp Date',widget=forms.TextInput(attrs={'placeholder':'MM/YY'}))
    ccv = forms.IntegerField(label='CCV')